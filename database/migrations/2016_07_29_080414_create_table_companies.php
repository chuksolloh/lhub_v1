<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanies extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('companies', function (Blueprint $table) {
            $table->increments('companyid');
			$table->string('company_name', 255);
			$table->enum('parent', [0, 1])->default(1);
			$table->text('address')->nullable();
			$table->text('logo')->nullable();
			$table->integer('stateid')->unsigned()->nullable();
			$table->string('city', 255)->nullable();
			$table->string('fb', 255)->nullable();
			$table->string('twitter', 255)->nullable();
			$table->string('linkedln', 255)->nullable();
			$table->string('instagram', 255)->nullable();
			$table->enum('blocked', [0, 1])->default(0);
            $table->timestamps();
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('companies');
    }
}
