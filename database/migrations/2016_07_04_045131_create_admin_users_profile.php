<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminUsersProfile extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_users_profile', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('userid')->unsigned();
			$table->string('first_name', 255)->nullable();
			$table->string('last_name', 255)->nullable();
			$table->text('address')->nullable();
			$table->string('phone', 255)->nullable();
			$table->integer('stateid')->nullable();
			$table->integer('countryid')->nullable();
            $table->timestamps();
			
			$table->foreign('userid')
				->references('userid')
				->on('admin_users')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('admin_users_profile');
    }
}
