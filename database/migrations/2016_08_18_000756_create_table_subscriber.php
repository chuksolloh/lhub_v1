<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSubscriber extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subscriber', function (Blueprint $table) {
            $table->increments('id');
			$table->integer('msisdn')->unsigned()->unique();
			$table->string('first_name', 255)->nullable();
			$table->string('last_name', 255)->nullable();
			$table->enum('gender', [1, 2, 3])->default(1);
			$table->date('dob')->nullable();
			$table->string('location')->nullable();
			$table->string('countryid', 255);
            $table->timestamps();
			
			$table->foreign('countryid')
				->references('countryid')
				->on('countries')
				->onUpdate('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('subscriber');
    }
}
