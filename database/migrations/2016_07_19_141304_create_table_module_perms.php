<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModulePerms extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_perms', function (Blueprint $table) {
            $table->increments('id_perms')->unsigned();
			$table->string('Permissions', 255);
			$table->integer('id_module')->unsigned();
			$table->string('id_string', 255);
			$table->enum('visible', [0, 1])->default(1);
			$table->integer('order')->unsigned();
            $table->timestamps();
			
			$table->foreign('id_module')
				->references('id_module')
				->on('modules')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('module_perms');
    }
}
