<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeyCompaniesForStates extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Add foreign key to companies table for states
		Schema::table('companies', function (Blueprint $table){
			$table->foreign('stateid')
				->references('stateid')
				->on('states');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropForeign('companies_stateid_foreign');
    }
}
