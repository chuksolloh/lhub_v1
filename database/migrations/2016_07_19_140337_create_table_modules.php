<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModules extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modules', function (Blueprint $table) {
            $table->increments('id_module')->unsigned();
			$table->string('module', 255);
			$table->string('id_string', 255);
			$table->integer('id_menu')->unsigned();
			$table->enum('visible', [0, 1])->default(1);
            $table->timestamps();
			
			$table->foreign('id_menu')
				->references('id_menu')
				->on('menus')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('modules');
    }
}
