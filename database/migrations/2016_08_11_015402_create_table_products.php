<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProducts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->increments('productid');
			$table->string('product_name', 255);
			$table->text('description')->nullable();
			$table->string('variant', 255)->default(1);
			$table->integer('brandid')->unsigned();
			$table->string('website', 255)->nullable();
			$table->string('fb', 255)->nullable();
			$table->string('twitter', 255)->nullable();
			$table->string('instagram', 255)->nullable();
			$table->enum('status', [0, 1])->default(1);
            $table->timestamps();
			
			$table->foreign('brandid')
				->references('brandid')
				->on('brands')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('products');
    }
}
