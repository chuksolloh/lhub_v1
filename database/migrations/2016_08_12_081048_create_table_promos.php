<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePromos extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('promos', function (Blueprint $table) {
            $table->increments('promoid');
			$table->string('title', 255);
			$table->text('description');
			$table->integer('reward_count')->unsigned()->default(0);
			$table->date('start_date');
			$table->integer('duration')->unsigned()->default(1);
			$table->integer('status')->unsigned()->default(1);
			$table->integer('productid')->unsigned();
			$table->integer('weight')->unsigned();
			$table->integer('reward_target')->unsigned()->default(0);
			$table->integer('reward_type')->unsigned()->default(1);
            $table->timestamps();
			
			$table->foreign('productid')
				->references('productid')
				->on('products')
				->onDelete('cascade');
			
			$table->foreign('reward_type')
				->references('rid')
				->on('rewards_types')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('promos');
    }
}
