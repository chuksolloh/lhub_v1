<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableBrands extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brands', function (Blueprint $table) {
            $table->increments('brandid');
			$table->string('brand_name', 255);
			$table->text('logo')->nullable();
			$table->text('description')->nullable();
			$table->integer('companyid')->unsigned();
			$table->enum('status', [0, 1])->default(1);
            $table->timestamps();
			
			$table->foreign('companyid')
				->references('companyid')
				->on('companies')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('brands');
    }
}
