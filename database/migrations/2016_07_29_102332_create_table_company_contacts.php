<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCompanyContacts extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_contacts', function (Blueprint $table) {
            $table->increments('contactid');
			$table->integer('companyid')->unsigned();
			$table->string('first_name', 255);
			$table->string('last_name', 255);
			$table->string('email', 255);
			$table->integer('code')->unsigned();
			$table->integer('mobile')->unsigned();
            $table->timestamps();
			
			$table->foreign('companyid')
				->references('companyid')
				->on('companies')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_contacts');
    }
}
