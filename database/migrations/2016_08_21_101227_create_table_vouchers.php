<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableVouchers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vouchers', function (Blueprint $table) {
            $table->increments('id');
			$table->string('v_code', 255);
			$table->string('batch', 255);
			$table->string('serial', 255);
			$table->datetime('activated_on')->nullable();
			$table->enum('v_status', [0, 1, 2])->default(0);
			$table->integer('weight')->unsigned()->default(0);
			$table->integer('promoid')->unsigned();
            $table->timestamps();
			
			$table->foreign('promoid')
				->references('promoid')
				->on('promos')
				->onDelete('cascade');
			
			$table->engine = 'InnoDB';
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('vouchers');
    }
}
