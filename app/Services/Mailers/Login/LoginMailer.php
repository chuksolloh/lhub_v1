<?php

namespace Services\Mailers;

use Services\Mailers\BaseMailer as Mailer;

/**
 * Description of LoginMailer
 *
 * @author Chuks
 */

class LoginMailer extends Mailer {
	//put your code here
	
	public static function sendForgotPWDEmail(array $data) {
		!empty($data) ? Mailer::sendMail('emails.admin.login.forgot_password', $data, 'Fotgot Password Request') : '';
	}
	
	public static function sendResetPasswordConfirmation(array $data = []) {
		Mailer::sendMail('emails.admin.login.password_reset', $data);
	}
}
