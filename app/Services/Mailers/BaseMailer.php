<?php

namespace Services\Mailers;

/**
 * Description of BaseMailer
 *
 * @author Chuks
 */

abstract class BaseMailer {
	//put your code here
	
	public static function sendMail($page, $data, $subject = 'Subject', $cc = '', $bcc = '', $attchments = array(), $exteraHeaders = array()) {
		\Mail::send($page, $data, function ($message) {
			$message->from('', '');
			$message->to();
			$message->subject($subject ? : 'No subject.');
		});
	}
}
