<?php

namespace Classes\Products;

use DB;

/**
 * Description of ProductsHandler
 *
 * @author Chuks
 */
class ProductsHandler {
	//put your code here
	const TABLE_PRODUCTS = 'products';
	
	public static function listAllProductsByCompanyId($cid) {
		if (is_integer($cid)) {
			return DB::table('products')
				->leftjoin('brands', 'brands.brandid', '=', 'products.brandid')
				->join('companies', 'brands.companyid', '=', 'companies.companyid')
				->select('products.*')
				->where('companies.companyid', '=', $cid)
				->where('products.status', '>', 0)
				->where('products.isdeleted', '=', 0)->get();
		}
		return false;
	}
	
	public static function listCompanyProductsByStatus($cid, $status = 1) {
		if (is_integer($status)) {
			return DB::table('products')
				->leftjoin('brands', 'brands.brandid', '=', 'products.brandid')
				->join('companies', 'brands.companyid', '=', 'companies.companyid')
				->select('products.*')
				->where('companies.companyid', '=', $cid)
				->where('products.status', '=', $status)->get();
		}
		return false;
	}
	
	public static function findProductById($id) {
		if (is_integer((int)$id)) { 
			return DB::table(self::TABLE_PRODUCTS)
				->where('productid', '=', (int)$id)
				->first();
		}
		return false;
	}
	
	public static function insert($data) { 
		if (!empty($data)) {
			return DB::table(self::TABLE_PRODUCTS)
				->insert(array(
				'product_name' => $data['product_name'],
				'description' => $data['description'],
				'variant' => $data['variant'],
				'brandid' => $data['brandid'],
				'website' => $data['website'] ? : '',
				'status' => $data['status'],
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now()
			));
		}
	}
	
	public static function update($data, $pid) {
		if (!empty($data)) {
			return DB::table(self::TABLE_PRODUCTS)
				->where('productid', '=', $pid)
				->update(array(
					'product_name' => $data['product_name'],
					'description' => $data['description'],
					'variant' => $data['variant'],
					'brandid' => $data['brandid'],
					'website' => $data['website'] ? : '',
					'status' => $data['status'],
					'updated_at' => \Carbon\Carbon::now()
				)
			);
		}
	}
	
	public static function productsToArray($cid) {
		$results = self::listCompanyProductsByStatus($cid); 
		
		$ret = [];
		
		if (!empty($results)) {
			foreach ($results as $res) {
				$ret[$res->productid] = $res->product_name;
			}
		}
		return $ret;
	}
}
