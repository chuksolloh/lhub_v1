<?php

namespace Classes\Admin\Menus;

use DB;

/**
 * Description of MenuHandler
 *
 * @author Chuks
 */

class MenuHandler {
	//put your code here
	protected $menustr = [];

	public function getMenuList() {
		try {
			$results = DB::table('menus')
				->leftjoin('modules', 'modules.id_menu', '=', 'menus.id_menu')
				->select('menus.menu', 'modules.module', 'modules.id_string')
				->where([
					['menus.visible', '=', 1],
					['modules.visible', '=', 1]
				])->get();
			
			return $this->buildMenuArrayList($results);
			
		} catch (Exception $ex) {
			throw new Exception($ex->getMessage());
		}
	}
	
	protected function buildMenuArrayList($items) {
		if (!empty($items)) {
			foreach ($items as $value) {
				$this->menustr[$value->menu][] = [$value->module => $value->id_string];
			}
			
			return $this->menustr;
		}
	}
}
