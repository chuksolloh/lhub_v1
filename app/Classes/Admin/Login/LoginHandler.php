<?php

namespace Classes\Admin\Login;

use \Interfaces\Login\LoginInterface;
use DB;
//use Session;
use Redis;

/**
 * Description of Login
 *
 * @author Chuks
 * 
 */

class LoginHandler implements LoginInterface{
	
	public static function auth($options) {
		if (!empty($options)) {
			try {
				$user = DB::table('admin_users')
					->join('admin_users_profile', 'admin_users.userid', '=', 'admin_users_profile.userid')
					->select('admin_users.*', 'admin_users_profile.*')
					->where('admin_users.email', $options['email'])
					->first();
			} catch(\Illuminate\Database\QueryException $ex) {
				dd($ex->getMessage());
			}
			return !empty($user) ? self::buildSessionVars($user) : false; // If user exists, add user details to session
		}
		return ;
	}
	
	protected static function buildSessionVars($data) { 
		if (!empty($data)) {
			try {
				Redis::hMset('user', array(
					'email' => $data->email,
					'name' => $data->first_name .' '. $data->last_name,
					'mobile' => $data->phone,
					'accttype' => $data->acct_type
				));
			} catch (\Predis\Connection\ConnectionException $ex) {
				dd($ex->getMessage());
			}
			return true;
		}
	}

	public static function logout() {
		Redis::flushall();//Session::flush(); 
		return true;
	}
	
	public static function register($params){
		// Register
	}
	
	public static function resetPWD($params) {
		// Reset password
	}
	
	protected static function encryptPWD($secret) {
		return $secret != '' ? Hash::make($secret) : ''; // Encrypt password with AES encryption via the Mcrypt PHP extension.
	}
}
