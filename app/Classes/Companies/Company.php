<?php

namespace Classes\Companies;

use DB;

/**
 * Description of CompanyHandler
 *
 * @author Chuks
 */

class Company extends \Exception {
	//put your code here
	const TABLE_COMPANIES = 'companies';
	const TABLE_COMPANY_CONTACTS = 'company_contacts';
	
	protected $blocked = array(0, 1);
	
	public static function getListedCompanies() {
		return DB::table(self::TABLE_COMPANIES)->get();
	}
	
	public static function fetchSelectedCompanyById($idCompany) {
		if (!empty($idCompany)) {
			return DB::table(self::TABLE_COMPANIES)
				->where('companyid', '=', $idCompany)->get();
		}
		
		return false;
	}
	
	public static function fetchCompanyProfileBySubdomain($subdomain) { 
		if ($subdomain != '') {
			return DB::table(self::TABLE_COMPANIES)
				->where('subdomain', '=', strtolower($subdomain))->first();
		}
		
		return false;
	}
	
	public static function addNewCompany($data) {
		if (!empty($data)) {
			$id = DB::table(self::TABLE_COMPANIES)->insertGetId(array(
				'company_name' => $data['company_name'],
				'address' => $data['address'],
				'logo' => $data['logo'],
				'stateid' => $data['stateid'],
				'city' => $data['city'],
				'fb' => $data['fb'],
				'twitter' => $data['twitter'],
				'linkedln' => $data['linkedin'],
				'instagram' => $data['instagram'],
				'subdomain' => $data['subdomain'],
			));
			
			return $id > 0 ? $this->addCompanysContactDetails($data, $id) : '';
		}
	}
	
	protected static function addCompanysContactDetails(array $data, $id) {
		if (!empty($data) && is_int($id)) {
			return DB::table(self::TABLE_COMPANY_CONTACTS)->insert(array(
				'companyid' => (int)$id,
				'first_name' => $data['first_name'],
				'last_name' => $data['last_name'],
				'email' => !filter_var($data['email'], FILTER_VALIDATE_EMAIL) ? NULL : $data['email'],
				'code' => !empty($data['code']) ? $data['code'] : 0,
				'mobile' => !empty($data['mobile']) ? $data['code'] : 0
			));
		}
	}
	
	public static function updateCompanyActivationStatus($idCompany, $status) {
		if (in_array($status, $this->blocked)) {
			return $this->update(self::TABLE_COMPANIES, array($idCompany), array('blocked' => intval($status)));
		}
	}
	
	protected static function update($table, $where, $updates, $column = 'companyid') {
		if ($table != '' && (is_array($where) && is_array($updates))) {
			return DB::table($table)
				->where($column, $where)
				->update($updates);
		}
		
		return false;
	}
}
