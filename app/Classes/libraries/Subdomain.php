<?php

namespace Classes\libraries;

use Redis;
use Classes\Companies\Company as Handler;

/**
 * Description of Subdomain
 *
 * @author Chuks
 */

class Subdomain {
	//put your code here
	private static $ids = array('www', 'portal', 'lhub');
	private static $subdomain;
	
	public static function init() { 
		self::initSubdomain();
		return Handler::fetchCompanyProfileBySubdomain(self::$subdomain);
	}
	
	protected static function initSubdomain() {
		$arrs = explode('.', $_SERVER['HTTP_HOST']);
		
		// www. call
		if (isset($arrs[1])) {
			if (strtolower($arrs[0]) == 'www' && !in_array($arrs[1], self::$ids)){
				self::$subdomain = $arrs[1];
				return true;
			}
		}
		// Local. call
		return self::$subdomain = in_array(self::$subdomain, self::$ids) ? false : $arrs[0];
	}
}
