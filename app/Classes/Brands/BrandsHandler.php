<?php

namespace Classes\Brands;

use DB;

/**
 * Description of BrandsHandler
 *
 * @author Chuks
 */

class BrandsHandler {
	//put your code here
	const TABLE_BRANDS = 'brands';
	
	public static function listAllBrandsByCompanyId($id) {
		if (!empty($id)) {
			return DB::table(self::TABLE_BRANDS)
				->where('companyid', '=', (int)$id)
				->where('isdeleted', '=', 0)
				->orderBy('created_at', 'desc')->get();
		}
		return false;
	}
	
	public static function listCompanyBrandsByStatus($companyid, $status = 1) {
		if (is_integer($companyid)) {
			return DB::table(self::TABLE_BRANDS)
				->where('companyid', '=', $companyid)
				->where('status', '=', $status)	
				->orderBy('created_at', 'desc')->get();
		}
		return false; 
	}
	
	public static function findBrandById($id) {
		if (is_integer((int)$id)) { 
			return DB::table(self::TABLE_BRANDS)
				->where('brandid', '=', (int)$id)
				->first();
		}
		return false;
	}
	
	public static function update($data, $id) {
		if (!empty($data)) {
			return DB::table(self::TABLE_BRANDS)
				->where('brandid', (int)$id)
				->update(['brand_name' => $data['brand_name'], 'description' => $data['description']]);
		}
		return false;
	}
	
	public static function insert($data) {
		return DB::table(self::TABLE_BRANDS)
			->insert([
				'brand_name' => $data['brand_name'],
				'description' => $data['description'],
				'companyid' => 1,
				'created_at' => \Carbon\Carbon::now(),
				'updated_at' => \Carbon\Carbon::now()
			]);
	}
	
	public static function delete($id) {
		return DB::table(self::TABLE_BRANDS)->where('brandid', (int)$id)->update(['isdeleted' => 1]);
	}
	
	public static function listBrandsByCompanyId($id) {
		if (!empty($id)) {
			$brands = self::listCompanyBrandsByStatus($id); 
			
			if (!empty($brands)) {
				$arr = [];
				foreach ($brands as $index => $value) {
					$arr[$value->brandid] = $value->brand_name;
				}
				return $arr;
			}
			return;
		}
	}
}
