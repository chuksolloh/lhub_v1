<?php

namespace Classes\Promos;

use DB;

/**
 * Description of PromoHandler
 *
 * @author Chuks
 * 
 * @property 
 */

class PromoHandler {
	const TABLE_PROMOS	= 'promos';
	const ONGOING		= 2;
	const DEFAULT_COUNT = 100;
	
	public static function listPromosByCompanyId($cid, $status = 2) {
		if (is_integer($cid)) {
			return DB::table('promos')
				->join('products', 'products.productid', '=', 'promos.productid')
				->join('brands', 'brands.brandid', '=', 'products.brandid')
				->join('companies', 'brands.companyid', '=', 'companies.companyid')
				->select('promos.*', 'companies.*')
				->where('companies.companyid', '=', $cid)
				->where('promos.status', '=', $status)->get();
		}
		return false;
	}
	
	public static function fetchPromoDetails($pid) {
		if (is_integer($pid)) {
			return DB::table(self::TABLE_PROMOS)->where('promoid', '=', $pid)->get();
		}
		return false;
	}
	
	public static function activePromos() {
		return DB::table('promos')
			->join('products', 'products.productid', '=', 'promos.productid')
			->join('brands', 'brands.brandid', '=', 'products.brandid')
			->join('companies', 'brands.companyid', '=', 'companies.companyid')
			->select('promos.*', 'companies.*')
			->where('promos.status', '=', self::ONGOING)->get();
	}
	
	public static function insert($data) {
		if (!empty($data)) {
			return DB::table(self::TABLE_PROMOS)
				->insert(array(
					'title' => $data['title'],
					'description' => $data['description'],
					'reward_count' => is_int($data['reward_count']) ? : self::DEFAULT_COUNT,
					'start_date' => '',
					'duration' => is_int($data['duration']) ? : 2,
					'productid' => $data['productid'],
					'weight' => is_int($data['weight']) ? : 0,
					'reward_target' => is_int($data['reward_target']) ? : 0,
					'reward_type' => $data['reward_type'],
					'created_at' => \Carbon\Carbon::now(),
					'updated_at' => \Carbon\Carbon::now()
				)
			);
		}
	}
	
	public function update() {
		
	}
	
	public function delete($id, $cid) {
		
	}
}
