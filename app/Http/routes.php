<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Home
//Route::get('/', function () {
//    return view('welcome');
//});

/* Home */
Route::get('/', 'Companies\CompanyController@index');
Route::get('home', 'Home\HomeController@index');

/* Admin Routes */
Route::get('admin/login', 'Admin\Login\LoginController@index');
Route::post('admin/auth', 'Admin\Login\LoginController@auth');
Route::get('admin/logout', 'Admin\Login\LoginController@logout');
Route::get('admin/dashboard', 'Admin\Login\LoginController@dashboard')->middleware(['lauth']);

Route::get('admin/companies', 'Admin\Companies\CompanyController@index');

/* Brands */
Route::resource('company/brands', 'Companies\BrandsController');

/* Products */
Route::resource('company/products', 'Companies\ProductsController');

/* Promos */
Route::resource('company/loyalities', 'Companies\LoyalitiesController');

/* Users */
Route::resource('users', 'Users\UserRepository');  