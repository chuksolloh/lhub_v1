<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use Classes\Admin\Menus\MenuHandler as Menus;

/**
 * Description of AdminModulesComposer
 *
 * @author Chuks
 */

class AdminModulesComposer {
	//put your code here
	protected $menus;
	protected $sidemenus = [];

	public function __construct(Menus $menus) {
		$this->menus = $menus->getMenuList();
	}
	
	public function compose(View $view) {
		$view->with('modules', $this->menus);
	}
}
