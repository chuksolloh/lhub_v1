<?php

namespace App\Http\Controllers\Admin\Login;

use Classes\Admin\Login\LoginHandler;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Request;
use Validator;
use Session;

/**
 * Description of LoginController
 *
 * @author Chuks
 */

class LoginController extends Controller {
	protected $request;

	public function __construct() {
		$this->request = new Request();
	}
	
	public function index() { 
		return view('admin.login.login');
	}
	
	public function dashboard() {
		return view('admin.login.dashboard');
	}
	
	public function auth() {
		$validator = Validator::make(Request::all(), array(
				'email' => 'required|email|exists:admin_users,email',
				'password' => 'required' ), array(
				'email.required' => 'We need to know your e-mail address!',
				'password.required' => 'No password supplied!'
			)
		);

		if ($validator->fails()) {
			return redirect('admin/login')
				->withErrors($validator)
				->withInput();
		} else {
			return LoginHandler::auth(Request::all()) != false ? redirect('admin/dashboard') : redirect('admin/login');
		}
	}
	
	public function forgotPassword() {
		// Forgot password goes here
	}
	
	public function register() {
		// Register other admins
	}
	
	public function resetPassword() {
		// Password reset
	}
	
	public function logout() {
		return LoginHandler::logout() ? redirect('admin/login') : back();
	}
}
