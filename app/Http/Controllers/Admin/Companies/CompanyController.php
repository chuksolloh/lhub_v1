<?php

namespace App\Http\Controllers\Admin\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Request;
use Validator;
use Classes\Companies\Company;

/**
 * Description of CompanyController
 *
 * @author Chuks
 */

class CompanyController extends Controller {
	//put your code here
	
	public function index() { 
		$lists = Company::getListedCompanies();
		$title = 'Companies';
		return view('admin.company.companies', compact('lists', 'title'));
	}
}
