<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use Classes\libraries\Subdomain as Subdomain;
use Classes\Promos\PromoHandler as Promo;

/**
 * Description of CompanyController
 *
 * @author Chuks
 */

class CompanyController extends Controller {
	//put your code here
	
	public function index() {
		$route = Subdomain::init();
		
		if (empty($route)) {
			return redirect('home');
		}
		
		$data = array(
			'promos' => Promo::listPromosByCompanyId($route->companyid)
		);
		
		return view('company.profile', $data);
	}
}
