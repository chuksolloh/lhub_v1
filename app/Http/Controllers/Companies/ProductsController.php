<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductRequest;
use Classes\Products\ProductsHandler as Product;
use Session;
use Classes\Brands\BrandsHandler as Brand;

/**
 * Description of ProductsController
 *
 * @author Chuks
 */
class ProductsController extends Controller {
	//put your code here
	
	public function __construct() {
		;
	}
	
	public function index() {
		$products = Product::listAllProductsByCompanyId(1);
		return view('company.products.list', compact('products'));
	} 
	
	public function show($id) {
		$products = Product::findProductById($id);
		return view('company.products.list', compact('products'));
	}
	
	public function create() {
		return view('company.products.create', array(
			'brands' => Brand::listBrandsByCompanyId(1)
		));
	}
	
	public function store(ProductRequest $request) { 
		$ret = Product::insert($request->all());
		$ret == true ? Session::flash('message', 'Successfully added a new product') : Session::flash('message', 'Failed to add new product');
		return redirect('company/products');
	}
	
	public function edit($id) { 
		return view('company.products.edit', array(
			'product' => Product::findProductById($id),
			'brands' => Brand::listBrandsByCompanyId(1)
		));
	}
	
	public function update($id, ProductRequest $request) { 
		Product::update($request->all(), $id);
		return redirect('company/products');
	}
	
	public function destroy($id) {
		Product::delete($id > 0 ? : 0);
		Session::flash('message', 'Successfully deleted product record');
		return redirect('company/products');
	}
}
