<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use Classes\Promos\PromoHandler as Promo;
use Session;
use Classes\Products\ProductsHandler as Product;

/**
 * Description of LoyalitiesController
 *
 * @author Chuks
 */
class LoyalitiesController extends Controller {
	/**
	 * 
	 * Show all active promos
	 */
	public function index() {
		$promos = Promo::listPromosByCompanyId(1);
		return view('company.loyalities.list', compact('promos'));
	}
	
	/**
	 * 
	 * @param type $id
	 */
	public function show($id) {
		
	}
	
	/**
	 * 
	 */
	public function create() {
		$product = Product::productsToArray(1);
		return view('company.loyalities.create', compact('product'));
	}
	
	/**
	 * 
	 * @param \App\Http\Controllers\Companies\Request $requests
	 */
	public function store(Request $request) {
		
	}
	
	/**
	 * 
	 * @param type $id
	 */
	public function edit($id) {
		
	}
	
	/**
	 * 
	 * @param type $id
	 * @param \App\Http\Controllers\Companies\Request $request
	 */
	public function update($id, Request $request) {
		
	}
	
	/**
	 * 
	 * @param type $id
	 */
	public function destroy($id) {
		
	}
}
