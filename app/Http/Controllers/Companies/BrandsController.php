<?php

namespace App\Http\Controllers\Companies;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use App\Http\Requests\BrandsRequest;
use Classes\Brands\BrandsHandler as Brand;
use Illuminate\Http\Request;
use Session;

/**
 * Description of BrandsController
 *
 * @author Chuks
 */

class BrandsController extends Controller {
	
	/**
	 * Display all company brands
	 * @return type
	 */
	public function index() {
		$brands = Brand::listAllBrandsByCompanyId(1);
		return view('company.brands.list', compact('brands'));
	} 
	
	public function show($id) {
		$brands = Brand::findBrandById($id);
		return view('company.brands.list', compact('brands'));
	}
	
	public function create() {
		return view('company.brands.create');
	}
	
	public function store(BrandsRequest $request) {
		$ret = Brand::insert($request->all());
		$ret == true ? Session::flash('message', 'Successfully added a new brand') : Session::flash('message', 'Failed to add new brand');
		return redirect('company/brands');
	}
	
	public function edit($id) { 
		$brand = Brand::findBrandById($id); 
		return view('company.brands.edit', compact('brand'));
	}
	
	public function update($id, BrandsRequest $request) { 
		Brand::update($request->all(), $id);
		return redirect('company/brands');
	}
	
	public function destroy($id) {
		Brand::delete($id > 0 ? : 0);
		Session::flash('message', 'Successfully deleted brand record');
		return redirect('company/brands');
	}
}
