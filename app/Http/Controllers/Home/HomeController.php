<?php

namespace App\Http\Controllers\Home;

use App\Http\Controllers\Controller;

/**
 * Description of HomeController
 *
 * @author Chuks
 */

class HomeController extends Controller {
	//put your code here
	
	public function index() {
		return view('pages.welcome');
	}
}
