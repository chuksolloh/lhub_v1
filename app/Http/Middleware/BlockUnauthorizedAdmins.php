<?php

namespace App\Http\Middleware;

use Closure;
use Redis;

class BlockUnauthorizedAdmins
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
		if (Redis::hGet('user', 'email') == null) {
			return redirect('admin/login');
		}
		
        return $next($request);
    }
}
