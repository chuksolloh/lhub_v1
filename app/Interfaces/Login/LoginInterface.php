<?php

namespace Interfaces\Login;

/**
 * Description of LoginInterface
 *
 * @author Chuks
 */

interface LoginInterface {
	public static function auth($params);
	public static function logout();
	public static function register($params);
	public static function resetPWD($params);
}
