<!DOCTYPE html>
<html>
	<head>
		<title></title>
		<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href="{{ URL::asset('bootstrap/css/bootstrap.css') }}" />
	</head>
	
	<body>
		<div class="container">
			<br/>
			<ul class="nav nav-pills">
				<li role="presentation" class="active"><a href="#">Dashboard</a></li>
				<li role="presentation" class=""><a href="#">Analysis</a></li>
				<li role="" class="dropdown">
					<a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
						Apps <span class="caret"></span>
						<ul class="dropdown-menu">
							<li><a href="{{ action('Companies\BrandsController@index') }}">Brands</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="{{ action('Companies\ProductsController@index') }}">Products</a></li>
							<li role="separator" class="divider"></li>
							<li><a href="{{ action('Companies\LoyalitiesController@index') }}">Loyalities</a></li>
						</ul>
					</a>
				</li>
			</ul>
		</div>

		<div class='container'>
			@yield('content')
		</div>
		
		<!-- jQuery 2.2.0 -->
		<script src="{{ URL::asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
		<!-- Bootstrap 3.3.6 -->
		<script src="{{ URL::asset('bootstrap/js/bootstrap.min.js') }}"></script>
	</body>
</html>