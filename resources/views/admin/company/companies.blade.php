@extends('admin.template.header')

@section('content')

<div class="pull-right">
	<button type="button" class="btn bg-olive margin">.btn.bg-olive</button>
</div>

<br/>

@if (!empty($lists))
	<div class="box">
		<div class="box-header">
			<h3 class="box-title">Data Table With Full Features</h3>
		</div>
		
		<div class="box-body">
			<table id="example1" class="table table-bordered table-striped">
				<thead>
					<tr>
						<th></th>
						<th>Company</th>
						<th>Status</th>
						<th>Activities</th>
						<th></th>
					</tr>
				</thead>
				
				<tbody>
					@foreach ($lists as $list) 
					<tr>
						<td><input type="checkbox"/></td>
						<td>{{ $list->company_name }}</td>
						<td>
							@if ($list->blocked == 0)
								<span class="label label-success">Active</span> 
							@else 
								<span class="label label-danger">Blocked</span>
							@endif
						</td>
						<td>
							<i class="fa fa-fw fa-bar-chart"></i>
						</td>
						<td>
							<i class="fa fa-fw fa-user"></i>
							<i class="fa fa-fw fa-edit"></i>
							<i class="fa fa-fw fa-trash"></i>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
@endif

<script src="{{ URL::asset('plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ URL::asset('plugins/datatables/dataTables.bootstrap.min.js') }}"</script>
<!-- SlimScroll -->
<script src="../../plugins/slimScroll/jquery.slimscroll.min.js"></script>

<script>
  $(function () {
    $("#example1").DataTable();
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false
    });
  });
</script>

@stop