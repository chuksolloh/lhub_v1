<!doctype html>
<html lang="en">
	<head>
		<title>IHUB Admin - Login</title>
		<!-- Css-files -->
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.min.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-theme.min.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('dist/css/AdminLTE.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('plugins/iCheck/square/blue.css') }}" />
	</head>
	
	<body class="hold-transition login-page">
		<div>
			@yield('content')
		</div>
		
		<div style="text-align: center">
			<?= 'Copyright '. date('Y') ?>
		</div>
		
		<!-- Js-files -->
		<script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap.js') }}"></script>
		<script src="{{ URL::asset('plugins/iCheck/icheck.min.js') }}"></script>
		<script src="{{ URL::asset('plugins/jQuery/jQuery-2.2.0.min.js') }}"></script>
		<script>
			$.material.init()
		</script>
	</body>
</html>
