<!doctype html>
<html lang="en">
	<head>
		<title>IHUB Admin - Login</title>
		<!-- Css-files -->
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-material-design.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-material-design.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-material-design.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/bootstrap-material-design.min.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/ripples.css.map') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/ripples.min.css') }}" />
		<link rel="stylesheet" href="{{ URL::asset('css/ripples.min.css.map') }}" />
	</head>
	
	<body>
		<div>
			@yield('content')
		</div>
		
		<br/><br/>
		<div style="text-align: center">
			<?= 'Copyright '. date('Y') ?>
		</div>
		
		<!-- Js-files -->
		<script src="{{ URL::asset('js/jquery.min.js') }}"></script>
		<script src="{{ URL::asset('js/material.js') }}"></script>
		<script src="{{ URL::asset('js/material.min.js') }}"></script>
		<script src="{{ URL::asset('js/ripples.js') }}"></script>
		<script src="{{ URL::asset('js/ripples.min.js') }}"></script>
		<script src="{{ URL::asset('js/bootstrap.js') }}"></script>
		<script>
			$.material.init()
		</script>
	</body>
</html>
