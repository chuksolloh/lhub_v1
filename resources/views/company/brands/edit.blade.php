@extends('templates.header')

@section('content')

<h1>Edit: {!! $brand->brand_name !!}</h1>

{!! Form::model($brand, ['method' => 'PATCH', 'action' => ['Companies\BrandsController@update', $brand->brandid]]) !!}
	@include ('company.brands.form', ['submitButtonText' => 'Update Brand'])
{!! Form::close() !!}

@include ('errors.brands.errors')

@stop
