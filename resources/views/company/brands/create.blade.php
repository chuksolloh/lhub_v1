@extends('templates.header')

@section('content')

<h1>Create a new brand</h1>

{!! Form::open(['url' => 'company/brands']) !!}
	@include ('company.brands.form', ['submitButtonText' => 'Add Brand'])
{!! Form::close() !!}

@include ('errors.brands.errors')

@stop