<div class="form-group">
	{!! Form::label('brand', 'Brand:') !!}
	{!! Form::text('brand_name', null, ['class' => 'form-control', 'placeholder' => 'Brand']) !!}
</div>

<div class="form-group">
	{!! Form::label('description', 'Description:') !!}
	{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
