@extends('templates.header')

@section('content')

<p>
	<br/>
	<a href="{{ URL::route('company.brands.create') }}">Add Brand</a>
</p>

@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (!empty($brands))
<table class="table">
	<thead>
		<tr>
			<th>Brand</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($brands as $brand)
		<tr>
			<td>{{ $brand->brand_name }}</td>
			<td>
				<a href="{{ action('Companies\BrandsController@edit', ['id' => $brand->brandid]) }}">Edit</a> |
				{{ Form::open(array('url' => 'company/brands/' . $brand->brandid, 'class' => 'pull-right')) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete brand', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endif

@stop
