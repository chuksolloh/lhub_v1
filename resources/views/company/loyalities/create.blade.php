@extends('templates.header')

@section('content')

<h1>Create a new loyality program</h1>

{!! Form::open(['url' => 'company/loyalities']) !!}
	@include ('company.loyalities.form', ['submitButtonText' => 'Add New Loyality'])
{!! Form::close() !!}

@include ('errors.loyalities.form-errors')

@stop