<div class="form-group">
	{!! Form::label('title', 'Title:') !!}
	{!! Form::text('title', null, ['class' => 'form-control', 'placeholder' => 'Loyality Title']) !!}
</div>

<div class="form-group">
	{!! Form::label('description', 'Description:') !!}
	{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
</div>

<div class="form-group">
	{!! Form::label('reward_count', 'Reward Count:') !!}
	{!! Form::text('reward_count', null, ['class' => 'form-control', 'placeholder' => 'Reward Count']) !!}
</div>

<div class="form-group">
	{!! Form::label('startdate', 'Start Date:') !!}
	{!! Form::text('start_date', null, ['class' => 'form-control', 'placeholder' => 'Start Date']) !!}
</div>

<div class="form-group">
	{!! Form::label('duration', 'Duration:') !!}
	{!! Form::text('duration', null, ['class' => 'form-control', 'placeholder' => 'Start Date']) !!}
</div>

<div class="form-group">
	{!! Form::label('productid', 'Product:') !!}
	{!! Form::select('productid', $product, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('weight', 'Weight:') !!}
	{!! Form::text('weight', null, ['class' => 'form-control', 'placeholder' => 'Weight']) !!}
</div>

<div class="form-group">
	{!! Form::label('reward_target', 'Reward Target:') !!}
	{!! Form::text('reward_target', null, ['class' => 'form-control', 'placeholder' => 'Reward Target']) !!}
</div>

<div class="form-group">
	{!! Form::label('reward_type', 'Reward Type:') !!}
	{!! Form::text('reward_type', null, ['class' => 'form-control', 'placeholder' => 'Reward Type']) !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
