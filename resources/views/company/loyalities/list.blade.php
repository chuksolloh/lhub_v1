@extends('templates.header')

@section('content')

<p>
	<br/>
	<a class="btn btn-success" href="{{ URL::route('company.loyalities.create') }}">Add New Loyality</a>
</p>

@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (!empty($promos))
<table class="table">
	<thead>
		<tr>
			<th>Promos</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($promos as $promo)
		<tr>
			<td>{{ $promo->title }}</td>
			<td>
				<a href="{{ action('Companies\LoyalitiesController@edit', ['id' => $promo->promoid]) }}">Edit</a> |
				{{ Form::open(array('url' => 'company/loyalities/' . $promo->promoid, 'class' => 'pull-right')) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete brand', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endif

@stop
