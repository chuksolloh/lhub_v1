@extends('templates.header')

@section('content')

<p>
	<br/>
	<a href="{{ URL::route('company.products.create') }}">Add Product</a>
</p>

@if (Session::has('message'))
	<div class="alert alert-info">{{ Session::get('message') }}</div>
@endif

@if (!empty($products))
<table class="table">
	<thead>
		<tr>
			<th>Products</th>
			<th>Action</th>
		</tr>
	</thead>
	<tbody>
		@foreach ($products as $product)
		<tr>
			<td>{{ $product->product_name }}</td>
			<td>
				<a href="{{ action('Companies\ProductsController@edit', ['id' => $product->productid]) }}">Edit</a> |
				{{ Form::open(array('url' => 'company/products/' . $product->productid, 'class' => 'pull-right')) }}
				{{ Form::hidden('_method', 'DELETE') }}
				{{ Form::submit('Delete brand', array('class' => 'btn btn-warning')) }}
				{{ Form::close() }}
			</td>
		</tr>
		@endforeach
	</tbody>
</table>
@endif

@stop
