<div class="form-group">
	{!! Form::label('product', 'Product:') !!}
	{!! Form::text('product_name', null, ['class' => 'form-control', 'placeholder' => 'Product']) !!}
</div>

<div class="form-group">
	{!! Form::label('description', 'Description:') !!}
	{!! Form::textarea('description', null, ['class' => 'form-control', 'placeholder' => 'Description']) !!}
</div>

<div class="form-group">
	{!! Form::label('variant', 'Variant:') !!}
	{!! Form::text('variant', null, ['class' => 'form-control', 'placeholder' => 'Variant']) !!}
</div>

<div class="form-group">
	{!! Form::label('brand', 'Brand:') !!}
	{!! Form::select('brandid', $brands, null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::label('website', 'Website Address:') !!}
	{!! Form::text('website', null, ['class' => 'form-control', 'placeholder' => 'Website Address']) !!}
</div>

<div class="form-group">
	{!! Form::label('status', 'Status:') !!}
	{!! Form::select('status', ['1' => 'Active', '0' => 'Inactive'], null, ['class' => 'form-control']) !!}
</div>

<div class="form-group">
	{!! Form::submit($submitButtonText, ['class' => 'btn btn-primary form-control']) !!}
</div>
