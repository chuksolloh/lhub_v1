@extends('templates.header')

@section('content')

<h1>Create a new product</h1>

{!! Form::open(['url' => 'company/products']) !!}
	@include ('company.products.form', ['submitButtonText' => 'Add Product'])
{!! Form::close() !!}

@include ('errors.products.form-errors')

@stop