@extends('templates.header')

@section('content')

<h1>Edit: {!! $product->product_name !!}</h1>

{!! Form::model($product, ['method' => 'PATCH', 'action' => ['Companies\ProductsController@update', $product->productid]]) !!}
	@include ('company.products.form', ['submitButtonText' => 'Update Product'])
{!! Form::close() !!}

@stop
