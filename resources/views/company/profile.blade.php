<p>
	Company Profile.
</p>

@if (!empty($promos)) 
	<h1>Available Promos</h1>
	<ul>
	@foreach ($promos as $promo) 
	<li>{{ $promo->title }}</li>
	@endforeach
	</ul>
@else
	<p>No promo currently running.</p>
@endif
               
